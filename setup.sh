#!/bin/bash

for lib_file in libs/*.sh
do
    . $lib_file
done

for config_file in *.config
do
    . $config_file
done

for step in setup-scripts/*-*.sh
do
    echo "------------------------------"
    head_echo "Processing $step"
    echo "------------------------------"
    . $step
done
