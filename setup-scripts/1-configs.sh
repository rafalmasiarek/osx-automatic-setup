#!/bin/bash


# check brew and update brew db
brew doctor
brew update

# add zsh to shells and make then as default to current user
head_echo "Add ZSH to shells and set as deafult for current user"
echo $(which zsh) | sudo tee -a /private/etc/shells
chsh -s $(which zsh)

# configure own zshrc 
head_echo "Configure own zsh settings"
echo "source ~/.my-own-zshrc" >> ~/.zshrc
touch ~/.my-own-zshrc
echo "export PATH=$HOME/bin:/usr/local/opt:/usr/local/sbin:/usr/local/bin:/Applications/Visual\ Studio\ Code.app/Contents/Resources/app/bin:$PATH" >> ~/.my-own-zshrc
echo "ZSH_THEME=\"fishy\"" >> ~/.my-own-zshrc
echo "HIST_STAMPS=\"dd-mm-yyyy\"" >> ~/.my-own-zshrc
echo "plugins=(git quotes gpg-keys-agent vscode)" >> ~/.my-own-zshrc
echo "export LANG=en_US.UTF-8" >> ~/.my-own-zshrc
echo "export PROMPT=\"%n@%m %d\"" >> ~/.my-own-zshrc

# install current bash version from cask and add to shell
head_echo "Install current bash version and add it to shells"
brew install bash
echo "Installed bash versions: $check_bash_installed"
echo $(brew --prefix)/bin/bash | sudo tee -a /private/etc/shells
