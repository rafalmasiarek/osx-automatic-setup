#!/bin/bash

if test -f ./brewapps.list ; then
    cat ./brewapps.list | while read -r app_name
    do
        head_echo " installing $app_name from brew"
        brew install $app_name
        $status=$?
    done
fi

if test -f ./caskapps.list ; then
    cat ./caskapps.list | while read -r app_name
    do
        head_echo " installing $app_name from brew cask"
        brew cask install $app_name
        $status=$?
    done
fi

if test -f ./appstoreapps.list ; then
    cat ./appstoreapps.list | while read -r app_name
    do
        head_echo " installing $app_name from AppStore"
        mas install $app_name
        $status=$?
    done
fi

if test -f ./npmeapps.list ; then
    cat ./appstoreapps.list | while read -r app_name
    do
        head_echo " installing $app_name by NodeJS"
        npm install $app_name
        $status=$?
    done
fi
