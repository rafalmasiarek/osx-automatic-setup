#!/bin/bash


mkdir -p ~/Development
mkdir -p ~/Development/apps
mkdir -p ~/Development/scripts
mkdir -p ~/Development/websites

sudo rm -rf /usr/local/etc/nginx
sudo rm -rf /usr/local/etc/php
sudo cp -r ./templates/nginx /usr/local/etc/nginx
sudo cp -r ./templates/php /usr/local/etc/php
sudo mv -f ./templates/dnsmasq.conf /usr/local/etc/

sudo brew services start dnsmasq
sudo brew services start php
sudo brew services install nginx
