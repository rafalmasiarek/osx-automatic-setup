#!/bin/bash

# ----------------------------------------------- TEXT

head_echo "Register Text editor"
echo "Marked default text editor is $defaultapp_text"
# register bundle id
bundle_id=`osascript -e "id of app \"$defaultapp_text\"" 2> /dev/null`
status=$?

# If exit status is true change default app for extensions from config file
if test $status -eq 1 ; then
    echo "$defaultapp_text doesn't exist. Fix the script or instiall this app first"
else
    for extension in "${extension_text[@]}"
    do
        echo -e "\n$extension will being open with $defaultapp_text ($bundle_id)\n"
        duti -s $bundle_id .$extension all
        echo -e "Here's proof:\n"
        echo -e "default app for opening $extension is:\n"
        duti -x $extension
        echo -e "\n----------"
    done
fi


# ----------------------------------------------- VIDEO

head_echo "Register video player"
echo "Marked default text editor is $defaultapp_video"
# register bundle id
bundle_id=`osascript -e "id of app \"$defaultapp_video\"" 2> /dev/null`
status=$?

# If exit status is true change default app for extensions from config file
if test $status -eq 1 ; then
    echo "$defaultapp_video doesn't exist. Fix the script or instiall this app first"
else
    for extension in "${extension_video[@]}"
    do
        echo -e "\n$extension will being open with $defaultapp_video ($bundle_id)\n"
        duti -s $bundle_id .$extension all
        echo -e "Here's proof:\n"
        echo -e "default app for opening $extension is:\n"
        duti -x $extension
        echo -e "\n----------"
    done
fi

# ----------------------------------------------- ARCHIVES

head_echo "Register (un)archiver"
echo "Marked default (un)archiver is $defaultapp_archive"
# register bundle id
bundle_id=`osascript -e "id of app \"$defaultapp_archive\"" 2> /dev/null`
status=$?

# If exit status is true change default app for extensions from config file
if test $status -eq 1 ; then
    echo "$defaultapp_archive doesn't exist. Fix the script or instiall this app first"
else
    for extension in "${extension_archive[@]}"
    do
        echo -e "\n$extension will being open with $defaultapp_archive ($bundle_id)\n"
        duti -s $bundle_id .$extension all
        echo -e "Here's proof:\n"
        echo -e "default app for opening $extension is:\n"
        duti -x $extension
        echo -e "\n----------"
    done
fi

# ----------------------------------------------- TORRENT

head_echo "Register torrent client"
echo "Marked default torrent client is $defaultapp_torrent"
# register bundle id
bundle_id=`osascript -e "id of app \"$defaultapp_torrent\"" 2> /dev/null`
status=$?

# If exit status is true change default app for extensions from config file
if test $status -eq 1 ; then
    echo "$defaultapp_torrent doesn't exist. Fix the script or instiall this app first"
else
    for extension in "${extension_torrent[@]}"
    do
        echo -e "\n$extension will being open with $defaultapp_torrent ($bundle_id)\n"
        duti -s $bundle_id .$extension all
        echo -e "Here's proof:\n"
        echo -e "default app for opening $extension is:\n"
        duti -x $extension
        echo -e "\n----------"
    done
fi


# ----------------------------------------------- TERMINAL

head_echo "Register terminal"
echo "Marked default terminal is $defaultapp_terminal"
# register bundle id
bundle_id=`osascript -e "id of app \"$defaultapp_terminal\"" 2> /dev/null`
status=$?

# If exit status is true change default app for extensions from config file
if test $status -eq 1 ; then
    echo "$defaultapp_terminal doesn't exist. Fix the script or instiall this app first"
else
    for extension in "${extension_terminal[@]}"
    do
        echo -e "\n$extension will being open with $defaultapp_terminal ($bundle_id)\n"
        duti -s $bundle_id .$extension all
        echo -e "Here's proof:\n"
        echo -e "default app for opening $extension is:\n"
        duti -x $extension
        echo -e "\n----------"
    done
fi
