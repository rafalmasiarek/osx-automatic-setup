#!/bin/bash

# xcode
head_echo "Install xcode"
xcode-select --install
sudo xcodebuild -license accept

# brew
head_echo "Install brew"
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

# zsh
head_echo "Install zsh"
brew install zsh
