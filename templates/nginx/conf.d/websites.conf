server {
    listen 80;
    server_name *.websites.dev.localhost;
    return 301 https://$host$request_uri;
}

server {

    listen  443 ssl;

    server_name ~^(?<subdomain>[^.]+).websites.dev.localhost;

    set $docroot /Users/rafalmasiarek/Development/websites/$subdomain;

    ssl_certificate /usr/local/etc/nginx/ssl/localhost.crt;
    ssl_certificate_key /usr/local/etc/nginx/ssl/localhost.key;

    root $docroot;

    index index.html index.php;

    location = /favicon.ico {
        log_not_found off;
        access_log off;
    }

    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }


    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~* ^.+.(html|jpg|jpeg|gif|css|png|js|ico|xml)$ {
        access_log        off;
        error_log         /dev/null crit;
        expires           -1;
        root $docroot;
    }

    location ~ .php$ {
        fastcgi_split_path_info ^(.+\.php)(.*)$;
        fastcgi_pass   127.0.0.1:9000;
        fastcgi_index  index.php;
        fastcgi_param  SCRIPT_FILENAME  $docroot$fastcgi_script_name;
        include fastcgi_params;
        fastcgi_param  QUERY_STRING     $query_string;
        fastcgi_param  REQUEST_METHOD   $request_method;
        fastcgi_param  CONTENT_TYPE     $content_type;
        fastcgi_param  CONTENT_LENGTH   $content_length;
        fastcgi_intercept_errors        on;
        fastcgi_ignore_client_abort     off;
        fastcgi_connect_timeout 60;
        fastcgi_send_timeout 180;
        fastcgi_read_timeout 180;
        fastcgi_buffer_size 128k;
        fastcgi_buffers 4 256k;
        fastcgi_busy_buffers_size 256k;
        fastcgi_temp_file_write_size 256k;
    }

    location ~ /\.ht {
        deny  all;
    }
}
